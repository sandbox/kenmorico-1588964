
-- SUMMARY --

Professional Share is a free Drupal module that is optimized for the social media buttons that professionals and B2B companies need most. It’s optimized for speed and analytics and uses blocks.

* Core buttons applicable to professionals and B2B – focus is on LinkedIn and Facebook action verb “recommend”.
* Full Google Analytics Social Tracking. Google Analytics reports successful shares only. Tracks shares, recommends, unlikes.
* Official button code from LinkedIn, Google, and Facebook.
* Allows Twitter username entry so Tweets can be attributed to you.
* Plugin buttons load once in the page speeding up the user experience.
* Allows custom Facebook AppID and administrator IDs for deeper Facebook integration.
* Creates 3 blocks for multiple placements.
* Show/hide buttons using block options.

Professional Share is also available as a WordPress plugin http://wordpress.org/extend/plugins/professional-share/

For a full description of the module, visit the project page:
  http://drupal.org/project/professional_share

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/professional_share


-- REQUIREMENTS --

Block module necessary.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Creates 3 blocks for multiple placements.
* Show/hide buttons using block options.


-- CUSTOMIZATION --

* Turn off block name using <none> or customize with your own.
* Buttons are in a div with a class called ProfessionalShareBox. Override the style if you like. 


-- TROUBLESHOOTING --




-- FAQ --

Q: Do I need Google Analytics?

A: No, but it is helpful.


-- CONTACT --

Current maintainers:
* Ken Morico - http://drupal.org/user/1960938

